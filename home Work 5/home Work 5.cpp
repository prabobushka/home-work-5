

#include <iostream>
void PrintNumbers(int N, bool Even)
{
    if (Even)
    {
        for (int i = 0; i < N; i = i + 2)
        {
            std::cout << "all even numbers:" << i << std::endl;
        }
    }
    else
    {
        for (int i = 1; i < N; i = i + 2)
        {
            std::cout << "all non-even numbers:" << i << std::endl;
        }

    }
}




int main()
{
    std::cout << "please enter a number:";
    int n;
    std::cin >> n;
    PrintNumbers(n, true);
    
    PrintNumbers(10, true); 
    PrintNumbers(10, false); 
}
 
